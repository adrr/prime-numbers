let totalPrimeNumberCount = 0;
let primeNumbers: number[] = [];
let nonPrimeNumbers: number[] = [];

for (let i = 1; i < 100; i++) {
    if (isPrime(i)) {
        totalPrimeNumberCount += 1;
        primeNumbers.push(i);
    } else {
        nonPrimeNumbers.push(i);
    }
}

console.log(`Prime numbers : ${primeNumbers}`);
console.log(`Non prime numbers: ${nonPrimeNumbers}`);
console.log(`Total prime numbers count is : ${totalPrimeNumberCount}`);

function isPrime(num: number): boolean {
    if (num < 2) return false;
    for (let n = 2; n < num; n++) {
        if (num % n === 0) return false;
    }
    return true;
}
