var totalPrimeNumberCount = 0;
var primeNumbers = [];
var nonPrimeNumbers = [];
for (var i = 1; i < 100; i++) {
    if (isPrime(i)) {
        totalPrimeNumberCount += 1;
        primeNumbers.push(i);
    }
    else {
        nonPrimeNumbers.push(i);
    }
}
console.log("Prime numbers : " + primeNumbers);
console.log("Non prime numbers: " + nonPrimeNumbers);
console.log("Total prime numbers count is : " + totalPrimeNumberCount);
function isPrime(num) {
    if (num < 2)
        return false;
    for (var n = 2; n < num; n++) {
        if (num % n === 0)
            return false;
    }
    return true;
}
